package com.luisgalvez.practica6convertidor

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import com.luisgalvez.practica6convertidor.databinding.ActivityMainBinding

class MainActivity : Activity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.text3.visibility = View.GONE
        binding.button.visibility = View.GONE
        binding.lvConversion1.visibility = View.GONE
        binding.lvConversion2.visibility = View.GONE
        binding.editTextNumber.visibility = View.GONE
        binding.btnReturn.visibility = View.GONE

        val arrayAdapter:ArrayAdapter<*>

        var tipoSeleccionado = ""

        val tipos = mutableListOf("Longitud", "Temperatura")
        val lvTipoConversion = findViewById<ListView>(R.id.lvTipoConversion)

        arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, tipos)
        lvTipoConversion.adapter = arrayAdapter

        lvTipoConversion.setOnItemClickListener(){parent, view, position, id->
            tipoSeleccionado = parent.getItemAtPosition(position).toString()
            val lvConversion1 = findViewById<ListView>(R.id.lvConversion1)

            val lvConversion2 = findViewById<ListView>(R.id.lvConversion2)

            if (tipoSeleccionado == "Longitud"){
                val arrayAdapterLonConversiones1:ArrayAdapter<*>
                val arrayAdapterLonConversiones2:ArrayAdapter<*>

                val tipoLongitud1 = mutableListOf("km", "m", "cm", "mm", "mi", "yd", "ft", "inch")
                val tipoLongitud2 = mutableListOf("km", "m", "cm", "mm", "mi", "yd", "ft", "inch")

                arrayAdapterLonConversiones1 = ArrayAdapter(this, android.R.layout.simple_list_item_1, tipoLongitud1)
                lvConversion1.adapter = arrayAdapterLonConversiones1


                arrayAdapterLonConversiones2 = ArrayAdapter(this, android.R.layout.simple_list_item_1, tipoLongitud2)
                lvConversion2.adapter = arrayAdapterLonConversiones2
            }
            else if (tipoSeleccionado == "Temperatura"){
                val arrayAdapterTempConversion1:ArrayAdapter<*>
                val arrayAdapterTempConversion2:ArrayAdapter<*>

                val tipoTemperatura1 = mutableListOf("°C", "°F", "°K")
                val tipoTemperatura2 = mutableListOf("°C", "°F", "°K")

                arrayAdapterTempConversion1 = ArrayAdapter(this, android.R.layout.simple_list_item_1, tipoTemperatura1)
                lvConversion1.adapter = arrayAdapterTempConversion1

                arrayAdapterTempConversion2 = ArrayAdapter(this, android.R.layout.simple_list_item_1, tipoTemperatura2)
                lvConversion2.adapter = arrayAdapterTempConversion2
            }


            binding.lvTipoConversion.visibility = View.GONE
            binding.textView.visibility = View.GONE
            binding.lvConversion1.visibility = View.VISIBLE
            binding.lvConversion2.visibility = View.VISIBLE
            binding.editTextNumber.visibility = View.VISIBLE
            binding.button.visibility = View.VISIBLE


            //Toast.makeText(this, parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show()
        }
        var longFrom = ""
        var longTo = ""

        binding.lvConversion1.setOnItemClickListener(){parent, view, position, id ->
            longFrom = parent.getItemAtPosition(position).toString()
        }

        binding.lvConversion2.setOnItemClickListener(){parent, view, position, id ->
            longTo = parent.getItemAtPosition(position).toString()
        }



        binding.button.setOnClickListener({
            binding.button.visibility = View.GONE
            binding.text3.visibility = View.VISIBLE
            binding.btnReturn.visibility = View.VISIBLE

            if (tipoSeleccionado == "Longitud"){
                if (longFrom == "mi"){
                    when (longTo){
                        "km" -> convertMiToKm()
                        "m" -> convertMiToMeters()
                        "cm" -> convertMiToCentimeters()
                        "mm" -> convertMiToMilimeters()
                        "mi" -> convertMiToMi()
                        "yd" -> convertMiToYard()
                        "ft" -> convertMiToFeat()
                        "inch" -> convertMiToInch()
                    }
                }else if (longFrom == "km"){
                    when (longTo){
                        "km" -> convertKmToKm()
                        "m" -> convertKmToMeters()
                        "cm" -> convertKmToCentimeters()
                        "mm" -> convertKmToMilimeters()
                        "mi" -> convertKmToMi()
                        "yd" -> convertKmToYard()
                        "ft" -> convertKmToFeat()
                        "inch" -> convertKmToInch()
                    }
                }else if (longFrom == "m"){
                    when (longTo){
                        "km" -> convertMToKm()
                        "m" -> convertMToMeters()
                        "cm" -> convertMToCentimeters()
                        "mm" -> convertMToMilimeters()
                        "mi" -> convertMToMi()
                        "yd" -> convertMToYard()
                        "ft" -> convertMToFeat()
                        "inch" -> convertMToInch()
                    }
                }else if (longFrom == "cm"){
                    when (longTo){
                        "km" -> convertCmToKm()
                        "m" -> convertCmToMeters()
                        "cm" -> convertCmToCentimeters()
                        "mm" -> convertCmToMilimeters()
                        "mi" -> convertCmToMi()
                        "yd" -> convertCmToYard()
                        "ft" -> convertCmToFeat()
                        "inch" -> convertCmToInch()
                    }
                }else if (longFrom == "mm"){
                    when (longTo){
                        "km" -> convertMmToKm()
                        "m" -> convertMmToMeters()
                        "cm" -> convertMmToCentimeters()
                        "mm" -> convertMmToMilimeters()
                        "mi" -> convertMmToMi()
                        "yd" -> convertMmToYard()
                        "ft" -> convertMmToFeat()
                        "inch" -> convertMmToInch()
                    }
                }else if (longFrom == "yd"){
                    when (longTo){
                        "km" -> convertYdToKm()
                        "m" -> convertYdToMeters()
                        "cm" -> convertYdToCentimeters()
                        "mm" -> convertYdToMilimeters()
                        "mi" -> convertYdToMi()
                        "yd" -> convertYdToYard()
                        "ft" -> convertYdToFeat()
                        "inch" -> convertYdToInch()
                    }
                }else if (longFrom == "ft"){
                    when (longTo){
                        "km" -> convertFtToKm()
                        "m" -> convertFtToMeters()
                        "cm" -> convertFtToCentimeters()
                        "mm" -> convertFtToMilimeters()
                        "mi" -> convertFtToMi()
                        "yd" -> convertFtToYard()
                        "ft" -> convertFtToFeat()
                        "inch" -> convertFtToInch()
                    }
                }else if (longFrom == "inch"){
                    when (longTo){
                        "km" -> convertInchToKm()
                        "m" -> convertInchToMeters()
                        "cm" -> convertInchToCentimeters()
                        "mm" -> convertInchToMilimeters()
                        "mi" -> convertInchToMi()
                        "yd" -> convertInchToYard()
                        "ft" -> convertInchToFeat()
                        "inch" -> convertInchToInch()
                    }
                }
            }else if(tipoSeleccionado == "Temperatura"){
                if (longFrom == "°C"){
                    when(longTo){
                        "°C" -> convertCToC()
                        "°F" -> convertCToF()
                        "°K" -> convertCToK()
                    }
                }else if (longFrom == "°F"){
                    when(longTo){
                        "°C" -> convertFToC()
                        "°F" -> convertFToF()
                        "°K" -> convertFToK()
                    }
                }else if (longFrom == "°K"){
                    when(longTo){
                        "°C" -> convertKToC()
                        "°F" -> convertKToF()
                        "°K" -> convertKToK()
                    }
                }
            }


        })

        binding.text3.setOnClickListener({
            binding.text3.visibility = View.GONE
            binding.button.visibility = View.VISIBLE
            binding.editTextNumber.setText("")
            binding.btnReturn.visibility = View.GONE
        })

        binding.btnReturn.setOnClickListener({
            binding.text3.visibility = View.GONE
            binding.button.visibility = View.GONE
            binding.lvConversion1.visibility = View.GONE
            binding.lvConversion2.visibility = View.GONE
            binding.editTextNumber.visibility = View.GONE
            binding.btnReturn.visibility = View.GONE
            binding.lvTipoConversion.visibility = View.VISIBLE
            binding.textView.visibility = View.VISIBLE
        })

    }

    /* Funciones para las medidas de millas a diferentes opciones */
    fun convertMiToKm(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1.609f

        binding.text3.text = "$resultado KM"

    }

    fun convertMiToMeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1609f


        binding.text3.text = "$resultado Mts"

    }

    fun convertMiToCentimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 160900f


        binding.text3.text = "$resultado Cms"

    }

    fun convertMiToMilimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1.609e+6f

        binding.text3.text = "$resultado mm"

    }

    fun convertMiToMi(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1f

        binding.text3.text = "$resultado Mi"

    }

    fun convertMiToYard(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1760f

        binding.text3.text = "$resultado Yd"

    }

    fun convertMiToFeat(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 5280f

        binding.text3.text = "$resultado Yd"

    }

    fun convertMiToInch(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 63360f

        binding.text3.text = "$resultado Inches"

    }

    /* Funciones para las medidas de Kilometros a diferentes opciones */
    fun convertKmToKm(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1f

        binding.text3.text = "$resultado Km"

    }

    fun convertKmToMeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1000f


        binding.text3.text = "$resultado Mts"

    }

    fun convertKmToCentimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 100000f


        binding.text3.text = "$resultado Cms"

    }

    fun convertKmToMilimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1e+6f

        binding.text3.text = "$resultado mm"

    }

    fun convertKmToMi(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 1609f

        binding.text3.text = "$resultado Mi"

    }

    fun convertKmToYard(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1094f

        binding.text3.text = "$resultado Yd"

    }

    fun convertKmToFeat(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 3281f

        binding.text3.text = "$resultado Yd"

    }

    fun convertKmToInch(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 39370f

        binding.text3.text = "$resultado Inches"

    }

    /* Funciones para las medidas de Metros a diferentes opciones */
    fun convertMToKm(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 1000f

        binding.text3.text = "$resultado Km"

    }

    fun convertMToMeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1f


        binding.text3.text = "$resultado Mts"

    }

    fun convertMToCentimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 100f


        binding.text3.text = "$resultado Cms"

    }

    fun convertMToMilimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1000f

        binding.text3.text = "$resultado mm"

    }

    fun convertMToMi(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 1609f

        binding.text3.text = "$resultado Mi"

    }

    fun convertMToYard(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1.094f

        binding.text3.text = "$resultado Yd"

    }

    fun convertMToFeat(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 3.281f

        binding.text3.text = "$resultado Yd"

    }

    fun convertMToInch(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 39.37f

        binding.text3.text = "$resultado Inches"

    }

    /* Funciones para las medidas de Centrimetros a diferentes opciones */
    fun convertCmToKm(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 100000f

        binding.text3.text = "$resultado Km"

    }

    fun convertCmToMeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 100f


        binding.text3.text = "$resultado Mts"

    }

    fun convertCmToCentimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1f


        binding.text3.text = "$resultado Cms"

    }

    fun convertCmToMilimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 10f

        binding.text3.text = "$resultado mm"

    }

    fun convertCmToMi(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 160900f

        binding.text3.text = "$resultado Mi"

    }

    fun convertCmToYard(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 91.44f

        binding.text3.text = "$resultado Yd"

    }

    fun convertCmToFeat(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 30.48f

        binding.text3.text = "$resultado Yd"

    }

    fun convertCmToInch(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 2.54f

        binding.text3.text = "$resultado Inches"

    }

    /* Funciones para las medidas de Milimetros a diferentes opciones */
    fun convertMmToKm(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 1e+6f

        binding.text3.text = "$resultado Km"

    }

    fun convertMmToMeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 1000f


        binding.text3.text = "$resultado Mts"

    }

    fun convertMmToCentimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 10f


        binding.text3.text = "$resultado Cms"

    }

    fun convertMmToMilimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1f

        binding.text3.text = "$resultado mm"

    }

    fun convertMmToMi(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 1.609e+6f

        binding.text3.text = "$resultado Mi"

    }

    fun convertMmToYard(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 914.4f

        binding.text3.text = "$resultado Yd"

    }

    fun convertMmToFeat(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 304.8f

        binding.text3.text = "$resultado Yd"

    }

    fun convertMmToInch(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 25.4f

        binding.text3.text = "$resultado Inches"

    }

    /* Funciones para las medidas de Yarda a diferentes opciones */
    fun convertYdToKm(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 1094f

        binding.text3.text = "$resultado Km"

    }

    fun convertYdToMeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 1.094f


        binding.text3.text = "$resultado Mts"

    }

    fun convertYdToCentimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 94.44f


        binding.text3.text = "$resultado Cms"

    }

    fun convertYdToMilimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 914.4f

        binding.text3.text = "$resultado mm"

    }

    fun convertYdToMi(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 1760f

        binding.text3.text = "$resultado Mi"

    }

    fun convertYdToYard(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1f

        binding.text3.text = "$resultado Yd"

    }

    fun convertYdToFeat(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 3f

        binding.text3.text = "$resultado Yd"

    }

    fun convertYdToInch(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 36f

        binding.text3.text = "$resultado Inches"

    }

    /* Funciones para las medidas de Pies a diferentes opciones */
    fun convertFtToKm(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 3281f

        binding.text3.text = "$resultado Km"

    }

    fun convertFtToMeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 3.281f


        binding.text3.text = "$resultado Mts"

    }

    fun convertFtToCentimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 30.48f


        binding.text3.text = "$resultado Cms"

    }

    fun convertFtToMilimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 304.8f

        binding.text3.text = "$resultado mm"

    }

    fun convertFtToMi(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 5280f

        binding.text3.text = "$resultado Mi"

    }

    fun convertFtToYard(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 3f

        binding.text3.text = "$resultado Yd"

    }

    fun convertFtToFeat(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1f

        binding.text3.text = "$resultado Yd"

    }

    fun convertFtToInch(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 12f

        binding.text3.text = "$resultado Inches"

    }

    /* Funciones para las medidas de Pulgadas a diferentes opciones */
    fun convertInchToKm(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 39370f

        binding.text3.text = "$resultado Km"

    }

    fun convertInchToMeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 39.37f


        binding.text3.text = "$resultado Mts"

    }

    fun convertInchToCentimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 2.54f


        binding.text3.text = "$resultado Cms"

    }

    fun convertInchToMilimeters(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 25.4f

        binding.text3.text = "$resultado mm"

    }

    fun convertInchToMi(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 63360f

        binding.text3.text = "$resultado Mi"

    }

    fun convertInchToYard(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() / 36f

        binding.text3.text = "$resultado Yd"

    }

    fun convertInchToFeat(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 12f

        binding.text3.text = "$resultado Yd"

    }

    fun convertInchToInch(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() * 1f

        binding.text3.text = "$resultado Inches"

    }

    /* Funciones para convertir las temperaturas de Grados Celsius a varios */
    fun convertCToF(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = (input.toFloat() * 9/5) + 32

        binding.text3.text = "$resultado °F"

    }

    fun convertCToK(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() + 273.15f

        binding.text3.text = "$resultado °K"

    }

    fun convertCToC(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat()

        binding.text3.text = "$resultado °C"

    }

    /* Funciones para convertir las temperaturas de Grados Fahrenheit a varios */
    fun convertFToF(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat()

        binding.text3.text = "$resultado °F"

    }

    fun convertFToK(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = (input.toFloat() - 32f) * 5/9 + 273.15f

        binding.text3.text = "$resultado °K"

    }

    fun convertFToC(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = (input.toFloat() - 32) * 5/9

        binding.text3.text = "$resultado °C"

    }

    /* Funciones para convertir las temperaturas de Grados Kelvin a varios */
    fun convertKToF(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = (input.toFloat() - 273.15f) * 9/5 + 32

        binding.text3.text = "$resultado °F"

    }

    fun convertKToK(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat()

        binding.text3.text = "$resultado °K"

    }

    fun convertKToC(){

        var input = binding.editTextNumber.getText().toString()

        var resultado:Float

        resultado = input.toFloat() - 273.15f

        binding.text3.text = "$resultado °C"

    }

}

